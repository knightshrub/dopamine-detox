# dopamine-detox

Potentially a misnomer, this script uses `/etc/hosts` to block access to any websites
that you always end up visiting instead of doing the tasks you were supposed to.
Coupled with `cron`, this is an effective method of enforcing limits on when
certain websites can be accessed.

The benefit of blocking using `/etc/hosts` is that it doesn't require any
special support from the network's DNS resolver, router etc.

The bash script only makes use of `sed` and `echo` and should run on pretty
much any Linux and BSD system.

## Installation

Edit the lists of websites to block in the script for example
```bash
sites=(youtube.com reddit.com twitter.com facebook.com news.ycombinator.com)
```

Place `dopamine-detox.sh` in a location of your choice, for example `/usr/local/bin`
and mark it executable
```
$ chmod +x dopamine-detox.sh
$ sudo mv dopamine-detox.sh /usr/local/bin
```

Add entries like the following to the root user's crontab.
The following crontab will block access to the listed sites between 8 AM to 12 PM
and 1 PM to 8 PM.  During the other times access to the websites is allowed.
```
*/5     8-11    *   *   *   /usr/local/bin/dopamine-detox.sh block /etc/hosts
*/5     13-19   *   *   *   /usr/local/bin/dopamine-detox.sh block /etc/hosts
*/5     0-7     *   *   *   /usr/local/bin/dopamine-detox.sh unblock /etc/hosts
*/5     12      *   *   *   /usr/local/bin/dopamine-detox.sh unblock /etc/hosts
*/5     20-23   *   *   *   /usr/local/bin/dopamine-detox.sh unblock /etc/hosts
```
