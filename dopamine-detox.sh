#!/bin/bash

#    Block access to websites using /etc/hosts
#    Copyright (C) 2021 Y. Ritterbusch
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# the websites to be blocked
sites=(youtube.com reddit.com twitter.com facebook.com news.ycombinator.com)

usage () {
    echo "Usage: $0 block|unblock FILE" 1>&2
    exit 1
}

block () {
    echo "# >>> dopamine-detox" >> $1
    for site in ${sites[@]}; do
        echo "127.0.0.1     $site" >> $1
        echo "::1           $site" >> $1
    done
    echo "# <<< dopamine-detox" >> $1
}

unblock () {
    sed -i '/# >>> dopamine-detox/,/# <<< dopamine-detox/d' $1
}

if [[ $# -ne 2 ]]; then
    echo "Invalid number of arguments: $#" 1>&2
    usage
fi

case $1 in
    block)
        unblock $2
        block $2
        ;;

    unblock)
        unblock $2
        ;;

    *)
        echo "Unrecognized command: $1" 1>&2
        usage
        ;;
esac
